

void XSleepAnotherWay(DWORD dwWaitInMSecs)
{

    DWORD dwStopTime = GetTickCount() + dwWaitInMSecs;

    DWORD dwTimeout = dwWaitInMSecs;

    MSG msg;

    while(1) {

        switch(MsgWaitForMultipleObjects(0, NULL, FALSE, dwTimeout, QS_ALLINPUT)) {

        case WAIT_TIMEOUT:

            break;

        default:

#ifdef __AFX_H__

            while(::PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE)) 

                AfxGetApp()->PumpMessage();

#else

            while(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {

                ::TranslateMessage(&msg);

                ::DispatchMessage(&msg);

            }

#endif

        }

        dwTimeout = dwStopTime - GetTickCount();

        if(!dwTimeout || dwTimeout>dwWaitInMSecs)

            break;

    }

}